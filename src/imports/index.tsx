import AbouteMe from "../components/aboutMe";
import Body from "../components/body";
import Bookmarks from "../components/bookmarks";
import Button from "../components/button";
import { Carousel } from "react-responsive-carousel";
import classnames from "classnames";
import Cv from "../components/cv";
import Experiences from "../components/experiences";
import Footer from "../components/footer";
import Gallery from "../components/gallery";
import { gsap } from "gsap";
import { Helmet, HelmetProvider } from "react-helmet-async";
import Head from "../components/head";
import Header from "../components/header";
import Heading from "../components/heading";
import Modal from "react-responsive-modal";
import Navigation from "../components/navigation";
import Paragraph from "../components/paragraph";
import Phone from "../components/phone";
import Presentation from "../components/presentation";
import Projects from "../components/projects";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Section from "../components/section";
import Services from "../components/services";
import Slider from "../components/slider";
import SocialMedia from "../components/socialMedia";
import Spinner from "../components/spinner";
import Square from "../components/square";
import Species from "../components/species";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import Technology from "../components/technology";
import Zip from "../components/zip";

import { useDataContext } from "../context";
import useModal from "../hooks/useModal";
import { useMediaQuery } from "react-responsive";
import usePage from "../hooks/usePage";
import useScrollFixed from "../hooks/useScrollFixed";
import useScrollTrigger from "../hooks/useScrollTrigger";
import useSlider from "../hooks/useSlider";
import useFadingContent from "../hooks/useFadingContent";

import { applicationsData } from "../data/applicationsData";
import { cvData } from "../data/cvData";
import formData from "../data/form.json";
import { projectsData } from "../data/projectsData";
import { servicesData } from "../data/servicesData";
import { technologyData } from "../data/technologyData";
import { websitesData } from "../data/websitesData";
import { allProjectsData } from "../data/allProjectsData";

import appleImage from "../assets/apple.png";
import myPhotoImage from "../assets/myPhoto.jpg";
import phoneImage from "../assets/phone2.png";
import sphereImage from "../assets/sphere.webp";
import squareImage from "../assets/square.png";

const imports = {
  AbouteMe,
  Body,
  Bookmarks,
  Button,
  Carousel,
  classnames,
  Cv,
  Experiences,
  Footer,
  Gallery,
  gsap,
  Head,
  Header,
  Heading,
  Helmet,
  HelmetProvider,
  Navigation,
  Paragraph,
  Phone,
  Presentation,
  Projects,
  ScrollTrigger,
  Section,
  Services,
  Slider,
  SocialMedia,
  Spinner,
  Square,
  Species,
  Technology,
  useDataContext,
  useFadingContent,
  useModal,
  usePage,
  useScrollFixed,
  useScrollTrigger,
  useSlider,
  websitesData,
  applicationsData,
  allProjectsData,
  projectsData,
  technologyData,
  cvData,
  formData,
  servicesData,
  LazyLoadImage,
  Modal,
  useMediaQuery,
  Zip,
  appleImage,
  myPhotoImage,
  phoneImage,
  sphereImage,
  squareImage
};

export default imports;
