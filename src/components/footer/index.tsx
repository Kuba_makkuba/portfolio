import imports from "../../imports";

const Footer = () => {

  const { AbouteMe, Navigation, SocialMedia, Heading, Section, useDataContext } = imports;

  const { refTechnology, refAwards, refContact, refPortfolio, refService, scrolltoref } = useDataContext();

  return (
    <footer>
      <Section>
        <div className="footer" >
          <AbouteMe about="Jakub Makowski 2021" text="All right reserved." />
        </div>
        <div className="footer">
          <div className="footer__wrapper">
          <Navigation>
            <li className="navigation__item" onClick={() => scrolltoref(refPortfolio, "My portfolio")}>My portfolio</li>
            <li className="navigation__item"onClick={() => scrolltoref(refService, "Service")}>Service</li>
            <li className="navigation__item" onClick={() => scrolltoref(refAwards, "Awards")}>Awards</li>
            <li className="navigation__item" onClick={() => scrolltoref(refTechnology, "Technology")}>Technology</li>
          </Navigation>
          </div>
        </div>
        <div className="footer footer__wrapper--up">
          <Navigation>
            <li className="navigation__item" onClick={() => scrolltoref(refContact, "My portfolio")}>Contact me</li>
            <Heading class="navigation__item" tag="li" headingText="Privacy" />
            <Heading class="navigation__item" tag="li" headingText="Terms & Conditions"/>
          </Navigation>
        </div>
        <div className="footer footer__wrapper--position">
          <SocialMedia />
        </div>
      </Section>
    </footer>
  );
};

export default Footer;
