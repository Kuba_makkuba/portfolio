import { Helmet } from "react-helmet-async";

const Head = () => {
    return (
        <Helmet>
            <link href="https://fonts.googleapis.com/css2?family=Outfit:wght@300&display=swap" rel="stylesheet"/>
            <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400&display=swap" rel="stylesheet"/>
            <link rel="canonical" href="https://heuristic-franklin-a9e0bb.netlify.app" />
            <meta property="heuristic-franklin-a9e0bb" content="https://heuristic-franklin-a9e0bb.netlify.app" />
            <meta name="description" content="Portfolio" />
            <meta name="theme-color" content="#101072"/>
            <meta name="Content-Type" content="text/html; charset=utf-8"/>
            <meta name="referrer" content="strict-origin" />
            <meta name="author" content="Jakub Makowski"/>
            <meta name="keywords" content="portfolio, project, frontend"/>
            <meta name="robots" content="index, follow"/>
            <meta charSet="UTF-8" http-equiv="X-UA-Compatible" name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover"/>
            <meta property="og:title" content="Portfolio"/>
            <meta property="og:description" content="Portfolio"/>
            <meta property="og:image" content="https://upload.wikimedia.org/wikipedia/commons/4/47/PNG_transparency_demonstration_1.png" />
            <title>Portfolio</title>
      </Helmet>
    )
}

export default Head;