import imports from "../../imports";
import { useState } from "react";

const Experiences = () => {

  const { Cv, Heading, Section, Button, Paragraph, Modal, useFadingContent } = imports;
  
  const [isContact, setIsContact] = useState<boolean>(false);

  const styles = {
    overlay: {
      padding: 0,
      margin : 0
    }
  };

  const contents = [
    "Jakub Makowski współpracuje przy projekcie 4agent od września 2023 roku. Jakub rozwija frontend aplikacji wykonany w react.js, tworzy generator dokumentów pdf oraz dba o działanie aplikacji na różnych urządzeniach.",
    "W trakcie realizacji projektu aplikacji dla agentów ubezpieczeniowych i ich klientów Jakub współpracuje z programistą backend oraz specjalistą devops. Aplikacja bowiem działa w oparciu o API, z których to metod Jakub korzysta programując rozwiązania UX.",
    "Jakub jest osobą godną zaufania, pełną pasji i zaangażowania oraz pomysłów.",
    "Współpraca z Jakubem pozwala rozwijać projekt i usprawniać pracę agentów ubezpieczeniowych i jakość obsługi ich klientów.",
  ];

  const { currentContent, fade } = useFadingContent(contents);

  return (
    <Section>
      <div className="experiences">
        <Heading tag="h2" headingText="Clients" class="heading__text heading__text--center" />
        <Heading tag="span" headingText="experience" class="heading__title"/>
        <div className="experiences__box">
          <div className="experiences__wrapper">
            <div className="experiences__item">
              <span className="experiences__wrapper experiences__item--topAngle" />
              <span className="experiences__wrapper experiences__item--topAngle" />
            </div>
            <div className="experiences__description">
              <article className={`experiences__content ${fade ? 'experiences__fadeIn' : 'experiences__fadeOut'}`}>
                <Paragraph text={currentContent} />
              </article>
            </div>
            <div className="experiences__item">
              <span className="experiences__wrapper experiences__item--bottomAngle" />
              <span className="experiences__wrapper experiences__item--bottomAngle" />
            </div>
          </div>
        </div>
        <div className="contact">
          <Button name="contact me" setIsOn={setIsContact} />
            <Modal open={isContact} onClose={() => setIsContact(false)} styles={styles} center>
                <Cv />
            </Modal>
        </div>
      </div>
    </Section>
  );
};

export default Experiences;
