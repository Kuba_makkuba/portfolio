export interface ProjectsType {
  name: string;
  description: string;
  images: string[];
  alts: string[];
}
