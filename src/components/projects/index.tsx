import { commercialProjectsData } from "../../data/commercialProjectsData";
import imports from "../../imports";
import { useEffect, useState, useRef } from "react";
import "react-responsive-modal/styles.css";
import { ProjectsType } from "./types";

const Projects = () => {
  const { Section, Button, Heading, projectsData, allProjectsData, LazyLoadImage, Modal,
  ScrollTrigger, gsap, useDataContext, Paragraph } = imports;

  const { index } = useDataContext();

  gsap.registerPlugin(ScrollTrigger);

  const [islinksProjects, setIsLinksProjects] = useState<boolean>(false);
  const [modal, setIsModal] = useState(false);

  useEffect(() => {
    if (modal) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "unset";
    }
  }, [modal]);

  const projectRefFirst = useRef<HTMLDivElement>(null);
  const projectRefSecond = useRef<HTMLDivElement>(null);
  const projectRefThird = useRef<HTMLDivElement>(null);

  useEffect(() => {
    gsap.fromTo(
      projectRefFirst.current,
      { autoAlpha: 0, x: "-=900" },
      {
        autoAlpha: 1,
        duration: 2,
        x: "=+0",
        transformOrigin: "left top",
        scrollTrigger: {
          trigger: projectRefFirst.current,
          start: "top 400px center",
        },
      }
    );
    gsap.fromTo(
      projectRefSecond.current,
      { autoAlpha: 0, x: "+=900" },
      {
        autoAlpha: 1,
        duration: 2,
        x: "=+0",
        transformOrigin: "right bottom",
        scrollTrigger: {
          trigger: projectRefSecond.current,
          start: "top 400px center",
        },
      }
    );
    gsap.fromTo(
      projectRefThird.current,
      { autoAlpha: 0, x: "-=900" },
      {
        autoAlpha: 1,
        duration: 2,
        x: "=+0",
        transformOrigin: "right bottom",
        scrollTrigger: {
          trigger: projectRefThird.current,
          start: "top 400px center",
        },
      }
    );
  }, [gsap]);

  const arrayRef = [projectRefFirst, projectRefSecond, projectRefThird];

  return (
    <Section row="row">
      <div className="projects">
        <div className="projects__main">
          <Heading tag="h2" headingText="My projects" class="heading__name" />
          <Button name="See all projects" setIsOn={setIsLinksProjects} index={index}/>
          {projectsData.map(({ name, image }, index) => (
            <div
              key={index}
              className="projects__wrapper"
              ref={arrayRef[index]}
            >
              <div className="projects__figure">
                <LazyLoadImage alt={name} src={image} className="projects__image" effect="blur"/>
              </div>
              <Button name="See more" setIsOn={setIsModal} index={index} />
            </div>
          ))}
          <Modal open={islinksProjects} onClose={() => setIsLinksProjects(false)} center>
            <div className="projects__list">
              {allProjectsData.map(({ image, name, link }, index) => (
                <div key={index} className="projects__box">
                  <div className="projects__figure">
                    <LazyLoadImage className="projects__picture" src={image} alt={name}/>
                    <a href={link} target="_blank" rel="noreferrer">
                      <Button name="See the project" setIsOn={setIsLinksProjects} nameClass={true}/>
                    </a>
                  </div>
                </div>
              ))}
            </div>
          </Modal>
          <Modal open={modal} onClose={() => setIsModal(false)} center>
            <div className="projects__modal">
              {allProjectsData[index] === undefined ? null : (
                <LazyLoadImage
                  className="projects__graphic"
                  src={allProjectsData[index].image}
                  alt={allProjectsData[index].name}
                />
              )}
              <div className="projects__page">
                {allProjectsData[index] === undefined ? null : (
                  <a
                    className="projects__link"
                    href={allProjectsData[index].link}
                    target="_blank"
                    rel="noreferrer"
                  >
                    Visit the website
                  </a>
                )}
              </div>
            </div>
          </Modal>
          <div className="projects__commercialTitleWrapper">
          <Heading tag="h2" headingText="Commercial projects" class="heading__name"/>
          </div>
          <div className="projects__commercial">
            {commercialProjectsData.map((item: ProjectsType, index) => (
              <div className="projects__commercialBox" key={index}>
                <article className="projects__commercialArticle">
                  <Paragraph classText="paragraph__textTwo" text={item.name} />
                  <Paragraph text={item.description} />
                </article>
                <div className="projects__commercialImages">
                  {item.images.map((img, idx: number) => (
                    <img
                      key={idx}
                      className={`projects__screen screen_${idx}`}
                      src={img}
                      alt={item.alts[index]}
                    />
                  ))}
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </Section>
  );
};

export default Projects;