interface ClassType {
  tag : keyof JSX.IntrinsicElements;
  spanClass ?: string;
  spanText ?: string;
  class ?: string;
  headingText ?: string;
  children ?: string;
}

const Heading = (props : ClassType) => {
    const Tag = props.tag
    const spanClass = props.spanClass === undefined ? '' : props.spanClass
    const span = props.spanText === undefined ? null : <span className={spanClass}>{props.spanText}</span>

    return <Tag className={props.class}>{props.headingText}{span}{props.children}</Tag>
  };
  
  export default Heading;