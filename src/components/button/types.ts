import { Dispatch, SetStateAction } from "react";

export interface ButtonType {
  name?: string;
  setIsOn: Dispatch<SetStateAction<boolean>>;
  nameClass?: Boolean;
  setIndex?: Dispatch<SetStateAction<number>>;
  index?: number;
}
