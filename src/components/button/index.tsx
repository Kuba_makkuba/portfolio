import { ButtonType } from "./types";
import imports from "../../imports";

const Button = ({name, setIsOn, nameClass, index} : ButtonType) => {

    const { useDataContext } = imports;

    const classes = nameClass === undefined ? "button" : "button button--position";
    const { setIndex } = useDataContext();

    const onModal = () => {
        setIsOn((prev : boolean) => !prev);
        setIndex(index || 0);
    }

    return(
        <button onClick={onModal} className={classes} >
            {name}
            <span className="button__arrow"/>
        </button>
    )
}

export default Button;