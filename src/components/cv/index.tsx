import imports from "../../imports";

const Cv = () => {

  const { Paragraph, SocialMedia, formData, Heading, LazyLoadImage, myPhotoImage } = imports;

  return (
    <div className="cv">
      <div className="cv__box">
        <div className="cv__figure">
          <LazyLoadImage alt={myPhotoImage} effect="blur" src={myPhotoImage} className="cv__photo"/>
        </div>
          <SocialMedia />
      </div>
      <div className="cv__informations">
        <article>
          <Heading spanClass="cv__title" headingText="A little bit about me" tag="h2"/>
          <Paragraph text="I am interested in programming but I am prepared for all kinds of technologies and practices. I cordially invite you to view all my projects."/>
        </article>
        <div className="cv__aboutMe">
         {Object.entries(formData[0]).map(([key, value]) => ( 
            <Paragraph classText="paragraph__textTwo" text={key} key={key} spanText={value}/>
         ))}
         <div className="cv__reference">
          <Paragraph classText="paragraph__textTwo" text="My references" />
          <a className="cv__link" href="./reference.pdf" download>download link</a>
         </div>
      </div>
     </div>   
    </div>
  );
};

export default Cv;