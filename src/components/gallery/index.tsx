import React, { useState } from "react";
import imports from "../../imports";

const Gallery = () => {

  const { Section, useDataContext, Paragraph, Carousel } = imports;
  const { page, numberProjects} = useDataContext();

  const [currentSlide, setCurrentSlide] = useState<number>(0);
  const [isLoad, setIsLoad] = useState(false);
  
  const photoBoard = Object.values(page[numberProjects]);
  photoBoard.splice(0,5);

  const updateCurrentSlide = (index: React.SetStateAction<number>) => {
    if (currentSlide !== index) {
      setCurrentSlide(index);
    }
  };

  const loading = () => {
    setIsLoad(true);
  }

  const photoBoardData: string[] = photoBoard as string[];

  return (
    <Section>
      <div className="gallery">
        <Carousel showThumbs={false} showArrows={false} showStatus={false} selectedItem={currentSlide} onChange={updateCurrentSlide} showIndicators={false}>
          {photoBoardData.map((item: string, index : number) => (
              <img key={index} className="gallery__image" src={item} alt={item} onLoad={loading}/>
          ))}
        </Carousel>
        {isLoad ? <div className="gallery__wrapper">
          <button onClick={() => setCurrentSlide(prev => prev - 1)} className="gallery__arrow">&larr;</button>
          <Paragraph classText="gallery__text" text={`${currentSlide + 1}/5`}/>
          <button onClick={() => setCurrentSlide(prev => prev + 1)} className="gallery__arrow">&rarr;</button>
        </div> : null}
      </div>
    </Section>
  );
};

export default Gallery;
