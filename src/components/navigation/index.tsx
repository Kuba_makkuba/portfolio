const Navigation = ({children} : { children : JSX.Element | JSX.Element[]}) => {
    return (
        <nav>
            <ul className="navigation__wrapper">
                {children}
            </ul>
        </nav>
    )
}

export default Navigation