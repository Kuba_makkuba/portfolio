import { AboutMeType } from "./types";
import imports from "../../imports";

const AbouteMe = ({about, text} : AboutMeType) => {

  const { Heading } = imports;

  return (
    <>
      <Heading tag="h1" headingText="J.M" class="heading__title" />
      <Heading tag="h2" headingText={about} class="heading__text" />
      <Heading tag="h2" headingText={text} class="heading__text" />
    </>
  );
};

export default AbouteMe;
