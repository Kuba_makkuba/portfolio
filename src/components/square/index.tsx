const Square = () => {
    return (
        <div className="square">
          <div className="square__wrapper">
            <h3 className="square__title">Engage Design Evolve</h3>
          </div>
        </div>      
    )
}

export default Square;