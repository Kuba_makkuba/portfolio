import imports from "../../imports";

const Bookmarks = () => {

  const { useModal, servicesData, classnames, Paragraph, LazyLoadImage } = imports;
  
  const { modelActive, extendedWindow, cl, arrow, clText } = useModal();

  return (
    <div className="bookmarks">
      {servicesData.map(({ classText, name, description, text, image }, index) => (
        <div key={index} className={extendedWindow === index ? cl : classnames("bookmarks__wrapper ", classText, "item")} onClick={() => modelActive(index, classText)}>
          <LazyLoadImage className="bookmarks__image" src={image} alt="ux"/>
          <Paragraph classText="bookmarks__number"> {classnames("0", index + 1)}
            <span className="bookmarks__title">{name}</span>
            <span className="bookmarks__description">{description}</span>
            <span className={extendedWindow === index ? clText : "bookmarks__desciptionAllOff"}>{text}</span>
          </Paragraph>
          <span className={extendedWindow === index ? classnames("bookmarks__arrow", arrow) : "bookmarks__arrow"}/>
        </div>
      ))}
    </div>
  );
};

export default Bookmarks;
