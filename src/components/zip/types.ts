export interface ZipType {
  isUp: boolean;
  isDown: boolean;
}