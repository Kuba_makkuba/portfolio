import imports from "../../imports";
import { ZipType } from "./types";

const Zip = ({isUp, isDown} : ZipType) => {

    const { Presentation, useDataContext } = imports;
    
    const { setNumberProjects } = useDataContext();

    return (
        <div className="zip">
          <div className="zip__arrows">
            <button className="zip__button" disabled={isUp} onClick={() => setNumberProjects((prev: number) => prev - 1)}>
              <span className="zip__arrowUp" />
            </button>
            <button className="zip__button" disabled={isDown} onClick={() => setNumberProjects((prev : number) => prev + 1)}>
              <span className="zip__arrowDown" />
            </button>
          </div>
          <Presentation />
        </div>
    )
}

export default Zip;