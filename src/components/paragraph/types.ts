import { ReactNode } from 'react';

export interface ParahraphType {
  text?: string;
  spanText?: string;
  classText?: string;
  children?: ReactNode;
}