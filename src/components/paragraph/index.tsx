import { ParahraphType } from "./types";

const Paragraph = (props : ParahraphType) => {
    
    const span = props.spanText ? props.spanText : null;
    const classText = props.classText ? props.classText : "paragraph__text";

    return(
        <p className={classText}>{props.text}{span}{props.children}</p>
    )
}

export default Paragraph;