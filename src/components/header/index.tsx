import imports from "../../imports";
import { useState } from "react";

const Header = () => {

  const { Navigation, AbouteMe, useDataContext, Square, LazyLoadImage, useScrollFixed, sphereImage } = imports;

  const [isMenu, setIsMenu] = useState<boolean>(false);

  const { refTechnology, scrolltoref, refPortfolio, refService, refAwards, refContact } = useDataContext();
  const { positionList } = useScrollFixed();

  const changeMenu = isMenu ? (
      <Navigation>
        <li className="navigation__item" onClick={() => scrolltoref(refPortfolio, "My portfolio")}>My portfolio</li>
        <li className="navigation__item" onClick={() => scrolltoref(refService, "Service")}>Service</li>
        <li className="navigation__item" onClick={() => scrolltoref(refAwards, "Awards")}>Awards</li>
        <li className="navigation__item" onClick={() => scrolltoref(refTechnology, "Technology")}>Technology</li>
        <li className="navigation__item" onClick={() => scrolltoref(refContact, "Contact me")}>Contact me</li>
      </Navigation>
  ) : null;

  const changeArrow = isMenu ? "navigation__active" : "navigation__notActive";

  return (
    <header>
      <div className="header">
        <LazyLoadImage alt="sphere" effect="blur" src={sphereImage} className="header__image"/>
        <div className="header__wrapper">
          <AbouteMe about="Jakub Makowski Portfolio" text="Front End Developer"/>
          <div className={positionList}>
              <h2 className="header__name">
                <span className="header__item" onClick={() => setIsMenu((prevState) => !prevState)}>Menu</span>
                <span className={changeArrow} />
              </h2>
              {changeMenu}
          </div>
        </div>
        <Square />
      </div>
    </header>
  );
};

export default Header;
