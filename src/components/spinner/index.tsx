const Spinner = () => {
  return (
    <div className="spinner">
      <div className="spinner__wrapper">
        <div className="spinner__item spinner__item--one"/>
        <div className="spinner__item spinner__item--two"/>
        <div className="spinner__item spinner__item--four"/>
        <div className="spinner__item spinner__item--three"/>
      </div>
    </div>
  );
};

export default Spinner;
