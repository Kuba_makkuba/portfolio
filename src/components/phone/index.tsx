import imports from "../../imports";

const Phone = () => {
  
  const { useDataContext, Gallery, useSlider, Section, Zip, phoneImage } = imports;

  const { page, setNumberProjects } = useDataContext();
  const { valueIndex, isUp, isDown } = useSlider(page[0].page);

  const designSelection = (index: number) => {
    setNumberProjects(index);
  };

  return (
    <Section>
      <div className="phone">
        <div className="phone__wrapper">
          <div className="phone__figure">
            <img className="phone__image" src={phoneImage} alt="phone"/>
          </div>
          <Gallery />
          <div className="phone__container">
            {page.map((_, index) => (
              <div onClick={() => designSelection(index)} key={index} className={`phone__item ${valueIndex === index ? " phone__item--color" : ""}`}/>
            ))}
          </div>
        </div>
        <Zip {...{isUp, isDown}}/>
      </div>
    </Section>
  );
};

export default Phone;
