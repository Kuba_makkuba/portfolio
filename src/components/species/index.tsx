import imports from "../../imports";

const Species = () => {

  const { Section, useDataContext, servicesData, classnames } = imports;
  
  const { choice } = useDataContext();

  return (
    <Section>
      <div className="species">
        {servicesData.map(({Nameclass, name, isNotActive}, index) => (
          <button onClick={() => choice(name)} key={index} className={classnames("species__button", Nameclass)} disabled={isNotActive}>
            <span>{name}</span>
            <span className="species__bar" />
          </button>
        ))}
      </div>
    </Section>
  );
};

export default Species;