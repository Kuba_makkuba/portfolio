import imports from "../../imports";

const Slider = () => {

  const { Heading, Section, Paragraph } = imports;

  return (
    <Section row="row">
      <Heading class="heading__text heading__text--center" tag="h2" headingText="My ideology is to engane my best skills and technology"/>
      <Heading class="heading__text heading__text--center" tag="h3" headingText="in the process of design"/>
      <div className="slider">
        <Paragraph text="to make your ideas evolve" classText="heading__title"/>
      </div>
    </Section>
  );
};

export default Slider;
