import imports from "../../imports";

const Presentation = () => {

  const { Heading, useDataContext, Paragraph, LazyLoadImage, appleImage } = imports;  

  const { page, numberProjects } = useDataContext();
  
  return (
    <div className="presentation">
      <div className="presentation__figure">
        <LazyLoadImage className="presentation__icon" src={page[numberProjects].icon} alt={page[numberProjects].title}/>
        <Heading class="presentation__name" headingText={page[numberProjects].title} tag="h3" />
      </div>
      <div className="presentation__informations">
        <article className="presentation__article">
          <Heading class="presentation__title" tag="h3" headingText={page[numberProjects].title} />
          <Paragraph text={page[numberProjects].text} classText="presentation__informations presentation__informations--color"/>
          <Paragraph text={page[numberProjects].description} classText="presentation__informations presentation__informations--bottom"/>
        </article>
      </div>
      <div className="presentation__download">
        <LazyLoadImage className="presentation__image" src={appleImage} alt="apple" />
        <button className="presentation__button">Download on the App Store</button>
      </div>
    </div>
  );
};

export default Presentation;
