import imports from "../../imports";

const SocialMedia = () => {

  const { cvData } = imports;

  return (
    <ul className="socialMedia">
      {cvData.map(({adress, image, alt}, index) => (
        <li key={index}>
          <a href={adress} target="_blank" rel="noreferrer">
            <img className="socialMedia__image" src={image} alt={alt}/>
          </a>
        </li>
      ))}
    </ul>
  );
};

export default SocialMedia;
