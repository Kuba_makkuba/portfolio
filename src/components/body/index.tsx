import imports from "../../imports";

const Body = () => {
  
  const { Footer, Header, Phone, Projects, Services, Slider, Species, Technology, useDataContext, Experiences } = imports;
  
  const { refTechnology, refAwards, refContact, refPortfolio, refService } = useDataContext();
  
  window.onbeforeunload = function () {
    window.scrollTo(0,0);
  }

  return (
    <>
      <Header />
      <div className="mainWrapper">
        <Slider />
      </div>
      <div className="mainWrapper">
        <Species />
      </div>
      <div className="mainWrapper">
        <Phone />
      </div >
      <div ref={refService} className="mainWrapper">
        <Services />
      </div>
      <div ref={refPortfolio} className="mainWrapper">
        <Projects />
      </div>
      <div ref={refTechnology} className="mainWrapper">
        <Technology />
      </div>
      <div ref={refAwards} className="mainWrapper">
        <Experiences />
      </div>
      <div ref={refContact} className="mainWrapper">
        <Footer />
      </div>
    </>
  );
};

export default Body;
