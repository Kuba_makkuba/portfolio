import imports from "../../imports";
import { useEffect, useRef } from "react";

const Technology = () => {

  const { Section, Heading, useDataContext, useScrollTrigger, Paragraph, technologyData, gsap, ScrollTrigger } = imports;

  const { refTechnology2 } = useDataContext();

  gsap.registerPlugin(ScrollTrigger);
  useScrollTrigger(refTechnology2);

  const revealRefs = useRef([]);

  useEffect(() => {
    revealRefs.current.forEach((el : never) => {
      gsap.to(el, {delay: 0.5, y: '-=70', duration: 2, autoAlpha: 1, scrollTrigger: { trigger: el,
      start : "top 750px",}});   

  },)
  }, [gsap]);

  function addToRefs(el : never) {
    if (el && !revealRefs.current.includes(el)) {
      revealRefs.current.push(el);
    }
  }

  return (
    <Section>
      <div className="technology" ref={refTechnology2}>
        <div className="technology__informations">
          <article>
            <Heading tag="h2" class="heading__name" headingText="Technology"/>
            <Paragraph text="I have essentail to use the most powerfull developers tools on the
              market. I want to makes sure that the solution i prepare form the
              client is the one that will make a difference and will enrich your
              facilites."/>
          </article>
        </div>
        <div className="technology__main">
        {technologyData.map(({text, image}, index) => (
          <div className="technology__box" key={index}>
              <div className="technology__wrapper">
                <span className="technology__block"/>
                <img className="technology__image" src={image} alt={text} ref={addToRefs}/>
              </div>
              <Paragraph text={text} classText="technology__name"/>
          </div>
        ))}
        </div >
      </div>
    </Section>
  );
};



export default Technology;