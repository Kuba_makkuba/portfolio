import imports from "../../imports";

const { Body, Head, HelmetProvider } = imports;

const App = () => {
  return (
    <HelmetProvider>
      <Head />
      <Body />
    </HelmetProvider>
  );
};

export default App;
