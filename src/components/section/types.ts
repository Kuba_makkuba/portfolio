export interface SectionType {
    row? : string;
    children? : JSX.Element | JSX.Element[];
}