import { SectionType } from "./types";

const Section = ({row, children} : SectionType) => {

    const flex = row === 'row' ? 'mainWrapperRow' : 'mainWrapperColumn';

    return (
        <div className={flex}>
            {children}
        </div>
    )
}

export default Section;