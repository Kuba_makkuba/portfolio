import imports from "../../imports";

const Services = () => {

  const { Section, Heading, Bookmarks, useDataContext , Paragraph, useScrollTrigger } = imports;

  const { refService2 } = useDataContext();
  useScrollTrigger(refService2);

  return (
    <Section row="row">
      <div className="services" ref={refService2}>
        <div className="services__description">
          <Heading class="heading__text" tag="h2" headingText="As a professional&#x2c; i can provide a wide range of services"/>
          <Paragraph text="to make sure yo have" classText="heading__text"/>
          <Paragraph text="everything under control" classText="heading__title"/>
        </div>
        <Bookmarks />
      </div>
    </Section>
  );
};

export default Services;
