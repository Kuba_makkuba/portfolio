import { useState, useEffect } from 'react';

const useFadingContent = (contents: string[]) => {

  const displayTime = 7000;
  const fadeTime = 1000;

  const [currentIndex, setCurrentIndex] = useState<number>(0);
  const [fade, setFade] = useState<boolean>(true);

  useEffect(() => {
    const interval = setInterval(() => {
      setFade(false);
      setTimeout(() => {
        setCurrentIndex((prevIndex) => (prevIndex + 1) % contents.length);
        setFade(true);
      }, fadeTime);
    }, displayTime + fadeTime);

    return () => clearInterval(interval);
  }, [contents.length, displayTime, fadeTime]);

  const currentContent = contents[currentIndex];

  return { currentContent, fade };
};

export default useFadingContent;