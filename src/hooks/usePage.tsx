import { useState } from "react";

const usePage = () => {
  
  const [pageIsActive, setPageIsActive] = useState<string>('');
  const [numberProjects, setNumberProjects] = useState<number>(0);
  const [components, setComponents] = useState<string>('');

  const choice = (item: string) => {

    setNumberProjects(0);
    if (item === "Websites") {
      setPageIsActive("Websites");
    } 
    else  {
      setPageIsActive("Applications");
    }
  };

  return { choice, pageIsActive, numberProjects, setNumberProjects, components, setComponents };
}

export default usePage;
