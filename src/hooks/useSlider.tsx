import { useState, useEffect } from "react";
import { useDataContext } from "../context";

const useSlider = (props : string) => {

    const { page, numberProjects } = useDataContext();

    const [isDown, setIsDown] = useState<boolean>(false);
    const [isUp, setIsUp] = useState<boolean>(false);
    
    const valueIndex = numberProjects;

    useEffect(() => {
        if (numberProjects === 0) {
            setIsDown(false);
            setIsUp(true);
        }
        if (numberProjects !== 0) {
            setIsDown(false);
            setIsUp(false);
        }
        if (numberProjects === page.length - 1) {
            setIsUp(false);
            setIsDown(true);
        }
        if (page.length === 1) {
            setIsDown(true);
            setIsUp(true);
        }
      },[numberProjects, page.length, props]);
      
      return { valueIndex, isUp, isDown };
}

export default useSlider;