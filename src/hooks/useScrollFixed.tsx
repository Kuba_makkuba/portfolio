import { useState, useEffect } from "react";
import { useMediaQuery } from 'react-responsive'

const useScrollFixed = () => {
  
  const [scrollTop, setScrollTop] = useState<number>(0);
  const [fixed, setFixed] = useState<boolean>(false);
  const [position, setPosition] = useState<number>(160);
  
  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-width: 1000px)'
  })

  const positionList = fixed ? "header__positionFixed" : "header__positionSticky";

  useEffect(() => {
    const onScroll = () => {
      setScrollTop(window.scrollY || window.pageYOffset);
    };
    
    window.addEventListener("scroll", onScroll);
    if (isDesktopOrLaptop) {
      setPosition(160);
    }
    if (!isDesktopOrLaptop) {
      setPosition(80);
    }
    if (scrollTop > position) {
      setFixed(true);
    } 
    else {
      setFixed(false);
    }
    return () => window.removeEventListener("scroll", onScroll);

  }, [scrollTop, isDesktopOrLaptop, position]);

  return { positionList, scrollTop };
};

export default useScrollFixed;