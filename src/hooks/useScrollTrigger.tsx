import { gsap, ScrollTrigger } from "gsap/all";
import { MutableRefObject, useEffect } from "react";

gsap.registerPlugin(ScrollTrigger);

const useScrollTrigger = ( props: MutableRefObject<HTMLDivElement | null> | any ) => {
  
  useEffect(() => {
    gsap.fromTo(
      props.current,
      { y: "100", autoAlpha: 0.5 },
      {
        y: "-=100",
        duration: 2,
        autoAlpha: 1,
        scrollTrigger: { trigger: props.current, start: "top center" },
      }
    );
  }, [props]);
};

export default useScrollTrigger;
