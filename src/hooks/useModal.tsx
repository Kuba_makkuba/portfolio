import { useState } from "react";

const useModal = () => {

    const [extendedWindow, setExtendedWindowIsActive] = useState<number>(100);
    const [cl, setCl] = useState<string>("bookmarks__wrapper");
    const [arrow, setArrow] = useState<string>('bookmarks__arrow');
    const [clText, setClText] = useState<string>("bookmarks__desciptionAllOff");

    const modelActive = (index : number, cls : string) => {
        setExtendedWindowIsActive(index);
        setCl(`bookmarks__wrapper ${cls}IsActive active`);
        setArrow(`bookmarks__arrow--rotate`);
        setClText("bookmarks__desciptionAll");
        
        if (cl === `bookmarks__wrapper ${cls}IsActive active`) {
            setCl(`bookmarks__wrapper ${cls} item back`);
            setArrow(`bookmarks__arrow`);
            setClText("bookmarks__desciptionAllOff");
        }
    }
    
    return { modelActive, extendedWindow, cl, arrow, clText };
}

export default useModal;