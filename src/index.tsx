import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { APIContextProvider } from './context';
import Spinner from './components/spinner';
import "./styles/themes/default/theme.scss";
import 'react-responsive-modal/styles.css';

const App = React.lazy(() => import("./components/app"));

ReactDOM.render(
  <React.StrictMode>
      <APIContextProvider>
        <Suspense fallback={<Spinner />}>
          <App />
        </Suspense>
      </APIContextProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

