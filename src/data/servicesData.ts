import applications from "../assets/applications.png";
import design from "../assets/design.png";
import software from "../assets/software.png";
import ux from "../assets/ux.png";

export const servicesData = [
  {
    classText: "bookmarks__wrapper--one",
    name: "UX/UI",
    text: "I'm in study mode.",
    image: ux,
    description: "Thorough resarch to prepare the best solutions.",
    Nameclass: "species__button--cursor",
    isNotActive: true,
  },
  {
    classText: "bookmarks__wrapper--two",
    name: "Websites",
    text: "I try to focus on details to make this type of website clear and pleasing to the eye for the client and users.",
    image: design,
    description: "Full responsive and equipped with modern technology.",
    Nameclass: "species__button--shade",
    isNotActive: false,
  },
  {
    classText: "bookmarks__wrapper--three",
    name: "Applications",
    text: "I can program applications the effect is very important to me in this type of applications I try to use the best solutions and methods I know to create applications.",
    image: applications,
    description:
      "Individually planned solutions to provide best answers to your problems and needs.",
    Nameclass: "species__button--shade",
    isNotActive: false,
  },
  {
    classText: "bookmarks__wrapper--four",
    name: "Software",
    text: "I'm in study mode.",
    image: software,
    description:
      "We examine structures of your company to find the best way to improve its operations.",
    Nameclass: "species__button--cursor",
    isNotActive: true,
  },
];
