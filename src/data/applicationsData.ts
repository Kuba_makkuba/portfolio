import ideas from "../assets/ideas.png";
import shoppingCart from "../assets/shopping-cart.png";

import quiz_1 from "../assets/quiz_1.webp";
import quiz_2 from "../assets/quiz_2.webp";
import quiz_3 from "../assets/quiz_3.webp";
import quiz_4 from "../assets/quiz_4.webp";
import quiz_5 from "../assets/quiz_5.webp";

import shop_1 from "../assets/shop_1.webp";
import shop_2 from "../assets/shop_2.webp";
import shop_3 from "../assets/shop_3.webp";
import shop_4 from "../assets/shop_4.webp";
import shop_5 from "../assets/shop_5.webp";

export const applicationsData = [
  {
    page: "Application",
    title: "Quiz",
    text: "try your hand at the quiz...",
    description: "quiz app I encourage you to test your skills by choosing your field and answering individual questions",
    icon: ideas,
    image_1: quiz_1,
    image_2: quiz_2,
    image_3: quiz_3,
    image_4: quiz_4,
    image_5: quiz_5,
  },
  {
    page: "Application",
    title: "Shop",
    text: "Shop design",
    description: "I allowed myself to create this project thanks to wix.com, which has such a website, but written differently :) the project is based on Handles CMS",
    icon: shoppingCart,
    image_1: shop_1,
    image_2: shop_2,
    image_3: shop_3,
    image_4: shop_4,
    image_5: shop_5,
  },
];
