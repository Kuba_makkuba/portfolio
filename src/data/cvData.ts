import facebook from "../assets/facebook.png";
import instagram from "../assets/instagram.png";
import linkedin from "../assets/linkedin.png";

export const cvData = [
  {
    image: facebook,
    alt: "facebook",
    adress: "https://www.facebook.com/jakub.makowski.127",
  },
  {
    image: instagram,
    alt: "instagram",
    adress: "https://www.instagram.com/makifs/",
  },
  {
    image: linkedin,
    alt: "linkedin",
    adress: "https://www.linkedin.com/in/jakub-makowski-b47211237/",
  },
];
