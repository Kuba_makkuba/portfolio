import app_1 from "../assets/app_1.webp";
import app_2 from "../assets/app_2.webp";
import app_3 from "../assets/app_3.webp";

export const projectsData = [
  {
    name: "Card",
    image: app_1,
    myProjects: "https://developerwebsite1-jm.netlify.app",
  },
  {
    name: "Quiz",
    image: app_2,
    myProjects: "https://quizapp-jm.netlify.app",
  },
  {
    name: "Shop",
    image: app_3,
    myProjects: "https://shop-app-alpha-dun.vercel.app",
  },
];
