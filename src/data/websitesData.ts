import card_1 from "../assets/card_1.webp";
import card_2 from "../assets/card_1.webp";
import card_3 from "../assets/card_1.webp";
import card_4 from "../assets/card_1.webp";
import card_5 from "../assets/card_1.webp";

import card2_1 from "../assets/card-2_1.webp";
import card2_2 from "../assets/card-2_2.webp";
import card2_3 from "../assets/card-2_3.webp";
import card2_4 from "../assets/card-2_4.webp";
import card2_5 from "../assets/card-2_5.webp";

import portfolio_1 from "../assets/portfolio_1.webp";
import portfolio_2 from "../assets/portfolio_2.webp";
import portfolio_3 from "../assets/portfolio_3.webp";
import portfolio_4 from "../assets/portfolio_4.webp";
import portfolio_5 from "../assets/portfolio_5.webp";

import portfolioIcon from "../assets/portfolioIcon.png";
import webIcon from "../assets/web.png";

export const websitesData = [
  {
    page: "Card",
    title: "Card",
    text: "Visiting site for a friend",
    description: "Website made by me for my friend who is a great programmer but he specializes in the back-end of various applications.",
    icon: webIcon,
    image_1: card_1,
    image_2: card_2,
    image_3: card_3,
    image_4: card_4,
    image_5: card_5,
  },
  {
    page: "Portfolio",
    title: "Portfolio",
    text: "My portfolio",
    description: "Another visit page, this time adjusted directly to my predispositions and technologies, I cordially invite you to view all my projects.",
    icon: portfolioIcon,
    image_1: portfolio_1,
    image_2: portfolio_2,
    image_3: portfolio_3,
    image_4: portfolio_4,
    image_5: portfolio_5,
  },
  {
    page: "Card 2",
    title: "Card 2",
    text: "Business card",
    description: "I invite you to check my latest project. A visiting page for a woman.",
    icon: webIcon,
    image_1: card2_1,
    image_2: card2_2,
    image_3: card2_3,
    image_4: card2_4,
    image_5: card2_5,
  },
];
