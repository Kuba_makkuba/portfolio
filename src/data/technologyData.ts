import api from "../assets/api.png";
import contentful from "../assets/contentful.png";
import css from "../assets/css.png";
import firebase from "../assets/firebase.png";
import git from "../assets/gitlab.png";
import gsap from "../assets/gsap.png";
import html from "../assets/html.png";
import js from "../assets/js.png";
import mongo from "../assets/mongo.png";
import next from "../assets/nextjs.jpg";
import node from "../assets/nodejs.png";
import react from "../assets/react.png";
import sass from "../assets/sass.png";
import sql from "../assets/sql.png";
import ts from "../assets/typescript.png";
import materialUI from "../assets/materialUI.png";
import antDesign from "../assets/antDesign.png";

export const technologyData = [
  {
    image: html,
    text: "html",
  },
  {
    image: css,
    text: "css",
  },
  {
    image: sass,
    text: "sass",
  },
  {
    image: js,
    text: "js",
  },
  {
    image: ts,
    text: "ts",
  },
  {
    image: react,
    text: "react",
  },
  {
    image: next,
    text: "next",
  },
  {
    image: gsap,
    text: "gsap",
  },
  {
    image: contentful,
    text: "contentful",
  },
  {
    image: git,
    text: "git",
  },
  {
    image: api,
    text: "api",
  },
  {
    image: firebase,
    text: "firebase",
  },
  {
    image: sql,
    text: "sql",
  },
  {
    image: node,
    text: "node",
  },
  {
    image: mongo,
    text: "mongo",
  },
  {
    image: materialUI,
    text: "mui",
  },
  {
    image: antDesign,
    text: "ant"
  }
];
