import app1 from "../assets/app_1.webp";
import app2 from "../assets/app_2.webp";
import app3 from "../assets/app_3.webp";
import app4 from "../assets/app_4.webp";
import app5 from "../assets/app_5.webp";

export const allProjectsData = [
  {
    name: "Card",
    image: app1,
    link: "https://developerwebsite1-jm.netlify.app",
  },
  {
    name: "Quiz",
    image: app2,
    link: "https://quizapp-jm.netlify.app",
  },
  {
    name: "Shop",
    image: app3,
    link: "https://shop-app-alpha-dun.vercel.app",
  },
  {
    name: "Card_2",
    image: app4,
    link: "https://developerwebsite2-jm.netlify.app",
  },
  {
    name: "My card",
    image: app5,
    link: "https://myportfolio-jm.netlify.app",
  },
];
