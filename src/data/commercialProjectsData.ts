import forAgent_1 from "../assets/forAgent_1.webp";
import forAgent_2 from "../assets/forAgent_2.webp";
import forAgent_3 from "../assets/forAgent_3.webp";

import pentabilities_1 from "../assets/pentabilities_1.webp";
import pentabilities_2 from "../assets/pentabilities_2.webp";
import pentabilities_3 from "../assets/pentabilities_3.webp";

export const commercialProjectsData = [
  {
    name: "4agent",
    description: "4agent enables remote management of the entire insurance sales cycle, from establishing contact with the customer to delivering a ready-made policy. Thanks to this application, you can remotely identify the client's needs, present him with the insurance conditions (GTC) and obtain all necessary consents. 4agent also streamlines the process of finalizing formalities related to distance sales, which makes it an ideal tool for insurance agents working remotely.",
    images: [forAgent_1, forAgent_2, forAgent_3],
    alts: ["forAgent_1", "forAgent_2", "forAgent_3"]
  },
  {
    name: "Pentabilities",
    description: "Pentabilities is an application supporting the socio-emotional development of students, focusing on five key skills: responsibility, cooperation, autonomy and initiative, thinking and managing emotions. The methodology includes designing educational activities, setting goals, observing and recording student behavior, analyzing data and reflecting on results. The app helps teachers build these skills in their classrooms, supporting students' holistic development.",
    images: [pentabilities_1, pentabilities_2, pentabilities_3],
    alts: ["pentabilities_1", "pentabilities_2", "pentabilities_3"]
  },
];
