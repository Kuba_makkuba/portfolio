import React,{ useContext, createContext, useRef, useState, SetStateAction, Dispatch, ReactNode } from "react";
import imports from "../imports";
import { PageDataType } from "./types";

type APIContextTypes = {
  choice: (a: string) => void;
  pageIsActive: string; 
  numberProjects: number;
  setNumberProjects: Dispatch<SetStateAction<number>>;
  components: string;
  setComponents: Dispatch<SetStateAction<string>>;
  page: PageDataType[];
  refTechnology: null | React.MutableRefObject<HTMLDivElement | null>;
  refTechnology2: null | React.MutableRefObject<HTMLDivElement | null>;
  refAwards: null | React.MutableRefObject<HTMLDivElement | null>;
  refContact: null | React.MutableRefObject<HTMLDivElement | null>;
  refPortfolio: null | React.MutableRefObject<HTMLDivElement | null>;
  refService: null | React.MutableRefObject<HTMLDivElement | null>;
  refService2: null | React.MutableRefObject<HTMLDivElement | null>;
  scrolltoref: ( ref: null | React.MutableRefObject<HTMLDivElement | null>, name : string ) => void;
  index: number,
  setIndex: Dispatch<SetStateAction<number>>;
};

const APIContext = createContext<APIContextTypes>({
  choice: () => {},
  pageIsActive: "",
  numberProjects: 0,
  setNumberProjects: () => {},
  components: "",
  setComponents: () => {},
  page: [],
  refTechnology: null,
  refTechnology2: null,
  refAwards: null,
  refContact: null,
  refPortfolio: null,
  refService: null,
  refService2: null,
  scrolltoref: () => {},
  index : 0, 
  setIndex : () => {}
})

export const APIContextProvider = ({ children }: { children: ReactNode} ) => {

  const { usePage, websitesData, applicationsData } = imports;
  const { choice, pageIsActive, numberProjects, setNumberProjects, components, setComponents } = usePage();
  const page = pageIsActive === "Websites" ? websitesData : applicationsData;

  const refTechnology = useRef(null);
  const refTechnology2 = useRef(null);
  const refAwards = useRef(null);
  const refContact = useRef(null);
  const refPortfolio = useRef(null);
  const refService = useRef(null);
  const refService2 = useRef(null);

  const scrolltoref = (ref: null | React.MutableRefObject<HTMLDivElement | null>) => {
    if (ref && ref?.current !== null) {
      ref.current.scrollIntoView({behavior: 'smooth'})
    }
  }

  const [index, setIndex] = useState(0)

  return (
    <APIContext.Provider
      value={{
        choice,
        pageIsActive,
        numberProjects,
        setNumberProjects,
        components,
        setComponents,
        page,
        refTechnology,
        refTechnology2,
        refAwards,
        refContact,
        refPortfolio,
        refService,
        refService2,
        scrolltoref,
        index, 
        setIndex
      }}
    >
      {children}
    </APIContext.Provider>
  );
};

export function useDataContext() {
  const context = useContext(APIContext);
  return context;
}