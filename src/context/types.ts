export interface PageDataType {
  page: string;
  title: string;
  text: string;
  description: string;
  icon: string;
  image_1: string;
  image_2: string;
  image_3: string;
  image_4: string;
  image_5: string;
}
